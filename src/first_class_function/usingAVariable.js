const sayHello = function () {
    return function () {
        console.log("Hello!");
    };
};

const myFunction = sayHello();

myFunction();

console.log("sayHello : ", sayHello);
console.log("sayHello() : ", sayHello());