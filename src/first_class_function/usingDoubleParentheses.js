function sayHello() {
    return function () {
        console.log("Hello!");
    };
}

sayHello()();

console.log("sayHello : ", sayHello);
console.log("sayHello() : ", sayHello());
console.log("sayHello()() : ", sayHello()());